# Advent of Code 2019

These are my (horrible) AOC 2019 solutions.

They are all written in a language called [Leviathan](https://github.com/chattahippie/Leviathan),
which is full of memory leaks, strange syntax, and absolutely
abysmal builtin functionality (only I/O is stdin/stdout).
