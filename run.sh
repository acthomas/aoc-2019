#!/bin/bash

# ./run.sh <day_num> <prob_num>

# Executes the problem with the given testcases.

main() {
  local day=$(printf "%02d" $1)
  local problem=$(printf "%02d" $2)

  cat $(dirname "$0")/$day/$problem/testdata.txt | /usr/local/bin/leviathan-1 $(dirname "$0")/$day/$problem/main.lev
}

main "$1" "$2"