# The Advent of Code Int code Computer

# Note this polutes the environment with all sorts of variables.
# It does so to save on memory down the line.
# Basically, shy away from using '_' as the start of var names.

V _doHaltFlags
V _programCounters
V _programs
V _doCompIo
V _compIo
V _compIoHead
V _compIoLen

# Math functions
V _mod = F(a,n) a - (n * int(a/n))
V _exp = F(n, e) I e > 0 n * _exp(n, e - 1) E 1

# Get an op code from an instruction.
V _opCode = F(n) _mod(n, 100)

# Get the parameter mode for a param from the instruction.
# p is 1-indexed: 1st param = 1, 2nd param = 2, ...
V _paramMode = F(n, p) _mod(int(n / _exp(10, p + 1)), 10)

# Get the value for a param.
# i is program list, p is param value (i[x]), m is param mode.
V _paramValue = F(i, p, m)
  I m==0
    # position mode
    i[p]
  E I m==1
    # immediate mode
    p

# n is the program number
V _pA V _pB V _pC V _op V _i V _c V _h
V _executeInstruction = F(n) {
  _i = _programs[n]
  _c = _programCounters[n]
  _h = _doHaltFlags[n]
  _op = _opCode(_i[_c])
  if _op == 99 {
    # Halt instruction (Day 02 Problem 01)
    _doHaltFlags[n] = T
  }
  else if _op == 1 {
    # Add instruction (Day 02 Problem 01)
    _pA = _paramValue(_i, _i[_c+1], _paramMode(_i[_c], 1))
    _pB = _paramValue(_i, _i[_c+2], _paramMode(_i[_c], 2))

    _i[_i[_c+3]] = _pA + _pB

    _programCounters[n]=_c+4
  }
  else if _op == 2 {
    # Multiply instruction (Day 02 Problem 01)
    _pA = _paramValue(_i, _i[_c+1], _paramMode(_i[_c], 1))
    _pB = _paramValue(_i, _i[_c+2], _paramMode(_i[_c], 2))

    _i[_i[_c+3]] = _pA * _pB

    _programCounters[n]=_c+4
  }
  else if _op == 3 {
    # Takes an input and writes it to memory
    # Day 05 Problem 01
    I _doCompIo {
      # only consume an input if one is available.
      # Otherwise, give up on this instruction cycle and try on next.
      I !(_compIoHead == _compIoLen) {
        _i[_i[_c+1]]=_compIo[_compIoHead]
        _compIoHead = _compIoHead + 1

        _programCounters[n]=_c+2
      }
    } E {
      PL("input")
      _i[_i[_c+1]]=int(input())
      _programCounters[n]=_c+2
    }
  }
  else if _op == 4 {
    # Outputs a value in memory.
    # Day 05 Problem 01
    _pA = _paramValue(_i, _i[_c+1], _paramMode(_i[_c], 1))
    
    I _doCompIo {
      _compIo[_compIoLen] = _pA
      _compIoLen = _compIoLen + 1
    } E
      PL(_pA)
    _programCounters[n]=_c+2
  }
  else if _op == 5 {
    # jump-if-true
    # Day 05 Problem 02
    _pA = _paramValue(_i, _i[_c+1], _paramMode(_i[_c], 1))
    _pB = _paramValue(_i, _i[_c+2], _paramMode(_i[_c], 2))

    I !(_pA == 0) _programCounters[n]=_pB
    E _programCounters[n]=_c+3
  }
  else if _op == 6 {
    # jump-if-false
    # Day 05 Problem 02
    _pA = _paramValue(_i, _i[_c+1], _paramMode(_i[_c], 1))
    _pB = _paramValue(_i, _i[_c+2], _paramMode(_i[_c], 2))

    I _pA == 0 _programCounters[n]=_pB
    E _programCounters[n]=_c+3
  }
  else if _op == 7 {
    # less than
    # Day 05 Problem 02
    _pA = _paramValue(_i, _i[_c+1], _paramMode(_i[_c], 1))
    _pB = _paramValue(_i, _i[c_+2], _paramMode(_i[_c], 2))

    I _pA < _pB
      _i[_i[_c+3]] = 1
    E
      _i[_i[_c+3]] = 0

    _programCounters[n]=_c+4
  }
  else if _op == 8 {
    # less than
    # Day 05 Problem 02
    _pA = _paramValue(_i, _i[_c+1], _paramMode(_i[_c], 1))
    _pB = _paramValue(_i, _i[_c+2], _paramMode(_i[_c], 2))

    I _pA == _pB
      _i[_i[_c+3]] = 1
    E
      _i[_i[_c+3]] = 0

    _programCounters=_c+4
  }
}

# Creates a computer ready to run 'num' number of programs simulaneously.
V setUpComputer = F(num) {
  # Set up initial state for halting flags
  _doHaltFlags = range(0, num)
  # Set up initial state for program counters
  _programCounters = range(0, num)

  resetComputer()
}

V resetComputer = F() {
  L i in range(0, len(_doHaltFlags)) _doHaltFlags[i] = false
  L i in range(0, len(_programCounters)) _programCounters[i] = 0
}

V _running V _runningProgramLoopCounter V _numPrograms
V runPrograms = F(programs, shouldUseCompIo, io, ioHead, ioLen) {
  _programs = programs
  _doCompIo = shouldUseCompIo
  _compIo = io
  _compIoHead = ioHead
  _compIoLen = ioLen
  _running = T
  _numPrograms = len(programs)

  W _running {
    _running = false
    _runningProgramLoopCounter = 0
    W _runningProgramLoopCounter < _numPrograms {
      _executeInstruction(_runningProgramLoopCounter)
      _running = _running or !(_doHaltFlags[_runningProgramLoopCounter])
      _runningProgramLoopCounter = _runningProgramLoopCounter + 1
    }
  }

  # Return where the head and len of compIo ended up after the program.
  ([_compIoHead, _compIoLen]);
}