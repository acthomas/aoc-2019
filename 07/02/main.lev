# AOC 2019 Day 7 Problem 2

U "../../utils/base.lev"
U "../../utils/string.lev"
U "../../utils/list.lev"

# This gives me the ability to "input" from the computer
V compIo = range(0,10000)
V compIoHead = 0
V compIoLen = 0
V doCompIo = T

# List of 'halt' flags. 1 per program running.
V halt = range(0,5)
L i in halt halt[i] = T
V progCounters = range(0,5)


V mod = F(a,n) a - (n * int(a/n))
V exp = F(n, e) I e > 0 n * exp(n, e - 1) E 1

V opCode = F(n) mod(n, 100)

# p is 1-indexed: 1st param = 1, 2nd param = 2, ...
V paramMode = F(n, p) mod(int(n / exp(10, p + 1)), 10)

# i is program list, p is param value (i[x]), m is param mode.
V paramValue = F(i, p, m)
  I m==0
    # position mode
    i[p]
  E I m==1
    # immediate mode
    p

# i is program/memory space, c is program counter
# returns how much to increment the program counter by
# h is the pointer to where in the halt array this program is.
V pA V pB V pC V op
V executeInstruction = F(i, c, h) {
  op = opCode(i[c])
  if op == 99 {
    # Halt instruction (Day 02 Problem 01)
    halt[h]=false 
  }
  else if op == 1 {
    # Add instruction (Day 02 Problem 01)
    pA = paramValue(i, i[c+1], paramMode(i[c], 1))
    pB = paramValue(i, i[c+2], paramMode(i[c], 2))

    i[i[c+3]] = pA + pB
    c=c+4
  }
  else if op == 2 {
    # Multiply instruction (Day 02 Problem 01)
    pA = paramValue(i, i[c+1], paramMode(i[c], 1))
    pB = paramValue(i, i[c+2], paramMode(i[c], 2))

    i[i[c+3]] = pA * pB
    c=c+4
  }
  else if op == 3 {
    # Takes an input and writes it to memory
    # Day 05 Problem 01
    I doCompIo {
      I !(compIoHead == compIoLen) {
        i[i[c+1]]=compIo[compIoHead]
        compIoHead = compIoHead + 1
        c=c+2
      }
    } E {
      i[i[c+1]]=int(input())
      c=c+2
    }
  }
  else if op == 4 {
    # Outputs a value in memory.
    # Day 05 Problem 01
    pA = paramValue(i, i[c+1], paramMode(i[c], 1))
    
    I doCompIo {
      compIo[compIoLen] = pA
      compIoLen = compIoLen + 1
    } E
      PL(pA)
    c=c+2
  }
  else if op == 5 {
    # jump-if-true
    # Day 05 Problem 02
    pA = paramValue(i, i[c+1], paramMode(i[c], 1))
    pB = paramValue(i, i[c+2], paramMode(i[c], 2))

    I !(pA == 0) c=pB
    E c=c+3
  }
  else if op == 6 {
    # jump-if-false
    # Day 05 Problem 02
    pA = paramValue(i, i[c+1], paramMode(i[c], 1))
    pB = paramValue(i, i[c+2], paramMode(i[c], 2))

    I pA == 0 c=pB
    E c=c+3
  }
  else if op == 7 {
    # less than
    # Day 05 Problem 02
    pA = paramValue(i, i[c+1], paramMode(i[c], 1))
    pB = paramValue(i, i[c+2], paramMode(i[c], 2))

    I pA < pB
      i[i[c+3]] = 1
    E
      i[i[c+3]] = 0

    c=c+4
  }
  else if op == 8 {
    # less than
    # Day 05 Problem 02
    pA = paramValue(i, i[c+1], paramMode(i[c], 1))
    pB = paramValue(i, i[c+2], paramMode(i[c], 2))

    I pA == pB
      i[i[c+3]] = 1
    E
      i[i[c+3]] = 0

    c=c+4
  }

  c
}

# i is the list of integers to execute as a program.
var runPrograms = function(programs) {
  V stillRunning = T
  V numProgs = len(programs)

  V i
    
  # Program Loop
  while stillRunning {
    stillRunning = false
    
    i = 0 W i < numProgs {
      progCounters[i] = executeInstruction(programs[i], progCounters[i], i)
      stillRunning = stillRunning O halt[i]
      i = i + 1
    }
  }
}

V main = F() {
  V ampProgram = map(split(input(), ","), F(e,i)int(e))
  V programA = map(ampProgram, F(e,i)e) # copy
  V programB = map(ampProgram, F(e,i)e) # copy
  V programC = map(ampProgram, F(e,i)e) # copy
  V programD = map(ampProgram, F(e,i)e) # copy
  V programE = map(ampProgram, F(e,i)e) # copy
  V programs = [programA, programB, programC, programD, programE]

  V settings = [5,6,7,8,9]

  
  V i
  V maxAmp = 0

  L a in settings
  L b in settings I !(b == a)
  L c in settings I !(c == a) A !(c == b)
  L d in settings I !(d == a) A !(d == b) A !(d == c)
  L e in settings I !(e == a) A !(e == b) A !(e == c) A !(e == d) {
    compIoHead = 0
    compIoLen = 6
    
    # Set up phase settings
    compIo[0] = a
    compIo[1] = b
    compIo[2] = c
    compIo[3] = d  
    compIo[4] = e
    # Initial amp value
    compIo[5] = 0

    # Reset the programs to initial state.
    i = 0 W i < len(ampProgram) {
      programA[i] = ampProgram[i]
      programB[i] = ampProgram[i]
      programC[i] = ampProgram[i]
      programD[i] = ampProgram[i]
      programE[i] = ampProgram[i]

      progCounters[0] = 0
      progCounters[1] = 0
      progCounters[2] = 0
      progCounters[3] = 0
      progCounters[4] = 0
      halt[0]=T
      halt[1]=T
      halt[2]=T
      halt[3]=T
      halt[4]=T
      i = i + 1
    }

    runPrograms(programs)
    #PL(compIo)

    I compIo[compIoLen - 1] > maxAmp maxAmp = compIo[compIoLen - 1]
  }

  PL(maxAmp)
}

main()